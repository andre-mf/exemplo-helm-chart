package com.example.pessoaapi.controllers;

import com.example.pessoaapi.domain.Pessoa;
import com.example.pessoaapi.domain.dto.PessoaDTO;
import com.example.pessoaapi.service.PessoaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "/pessoas")
public class PessoaController {

    @Autowired
    private PessoaService service;

    @GetMapping(path = "/all")
    public ResponseEntity<List<PessoaDTO>> findAll() {
        List<Pessoa> list = service.findAll();
        List<PessoaDTO> listDto = list.stream().map(PessoaDTO::new).collect(Collectors.toList());
        return ResponseEntity.ok().body(listDto);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<PessoaDTO> findById(@PathVariable String id) {
        Pessoa obj = service.findById(id);
        return ResponseEntity.ok().body(new PessoaDTO(obj));
    }

    @PostMapping
    public ResponseEntity<Void> insert(@RequestBody PessoaDTO objDto) {
        Pessoa obj = service.fromDto(objDto);
        service.insert(obj);
        URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(obj.getId()).toUri();
        return ResponseEntity.created(uri).build();
    }

    @PutMapping(value = "/{id}")
    public ResponseEntity<Void> update(@PathVariable String id, @RequestBody PessoaDTO objDto) {
        Pessoa obj = service.fromDto(objDto);
        obj.setId(id);
        service.update(obj);
        return ResponseEntity.noContent().build();
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Void> delete(@PathVariable String id) {
        service.delete(id);
        return ResponseEntity.noContent().build();
    }
}
