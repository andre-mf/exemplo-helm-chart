package com.example.pessoaapi.service;

import com.example.pessoaapi.domain.Pessoa;
import com.example.pessoaapi.domain.dto.PessoaDTO;
import com.example.pessoaapi.repository.PessoaRepository;
import com.example.pessoaapi.service.exception.ObjectNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class PessoaService {

    @Autowired
    private PessoaRepository repository;

    public List<Pessoa> findAll() {
        return repository.findAll();
    }

    public Pessoa findById(String id) {
        Optional<Pessoa> pessoa = repository.findById(id);
        return pessoa.orElseThrow(() -> new ObjectNotFoundException("Objeto não encontrado - id " + id));
    }

    public Pessoa insert(Pessoa obj) {
        return repository.insert(obj);
    }

    public void delete(String id) {
        findById(id);
        repository.deleteById(id);
    }

    public Pessoa update(Pessoa obj) {
        Pessoa newObj = findById(obj.getId());
        updateData(newObj, obj);
        return repository.save(newObj);
    }

    public void updateData(Pessoa newObj, Pessoa obj) {
        newObj.setNome(obj.getNome());
        newObj.setIdade(obj.getIdade());
    }

    public Pessoa fromDto(PessoaDTO objDto) {
        return new Pessoa(objDto.getId(), objDto.getNome(), objDto.getIdade());
    }
}
