package com.example.pessoaapi.domain;

import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Document
public class Pessoa {

    @Id
    private String id;
    private String nome;
    private Integer idade;
}
