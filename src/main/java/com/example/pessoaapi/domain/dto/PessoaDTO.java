package com.example.pessoaapi.domain.dto;

import com.example.pessoaapi.domain.Pessoa;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class PessoaDTO {

    private String id;
    private String nome;
    private Integer idade;

    public PessoaDTO(Pessoa obj) {
        this.id = obj.getId();
        this.nome = obj.getNome();
        this.idade = obj.getIdade();
    }
}
