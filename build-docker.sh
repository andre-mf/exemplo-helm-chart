#!/bin/bash

# Geração do pacote JAR da aplicação
./mvnw package -Dmaven.test.skip=true

# Build da imagem Docker
docker build -t andremf/pessoa-api:latest -f src/main/resources/Docker/Dockerfile target/

# Push da imagem
docker push andremf/pessoa-api:latest

# Remoção da imagem
docker image rm andremf/pessoa-api:latest

# Remoção do diretório target
rm -rf target/